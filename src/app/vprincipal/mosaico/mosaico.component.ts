import { ProyectoComponent } from './../../vproyecto/proyecto/proyecto.component';
import { Component, OnInit, Input, HostListener, ViewChild } from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
@Component({
  selector: 'app-mosaico',
  templateUrl: './mosaico.component.html',
  styleUrls: ['./mosaico.component.css']
})
export class MosaicoComponent implements OnInit {
  ProyectoSeleccionado  = false;
  static: boolean;
  constructor() { }
 @Input() vlProyecto: ProyectoComponent;
  ngOnInit() {
  }

  abrirProyecto() {
     this.ProyectoSeleccionado = true;
  }
  cerrarProyecto() {
    this.ProyectoSeleccionado = false;
 }
}
