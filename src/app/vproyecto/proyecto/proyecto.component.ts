import { LiderComponent } from './../../vlider/lider.component';
import { Component, OnInit, Input, HostBinding } from '@angular/core';
import {ProyectoService} from '../../servicios/proyecto.service';
import { Proyecto } from '../../metodos/proyecto';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { ValtaproyectoComponent } from '../alta/valtaproyecto.component';
import { AreaComponent } from '../../varea/area.component';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.css']
})
export class ProyectoComponent implements OnInit {
  isPopupOpened = true;
  proyecto: Proyecto = new Proyecto();
  constructor( private vlProyectoService: ProyectoService, private dialog: MatDialog, private vlRoute: Router) { }

  ngOnInit() {
    this.getProyecto();
  }
    getProyecto() {
      this.vlProyectoService.getProyecto().subscribe(
        (proyecto) => {
          this.proyecto = proyecto;
        });
    }
    onCreate() {
      /*this.service.initializeFormGroup();*/
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '60%';
      this.dialog.open(LiderComponent, dialogConfig);
    }
    addLider() {
      this.isPopupOpened = true;
      const dialogRef = this.dialog.open(LiderComponent, {
        data: {}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.isPopupOpened = false;
      });
    }
    addArea() {
      this.isPopupOpened = true;
      const dialogRef = this.dialog.open(AreaComponent, {
        data: {}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.isPopupOpened = false;
      });
    }

    addProyecto() {
      this.isPopupOpened = true;
      const dialogRef = this.dialog.open(ValtaproyectoComponent, {
        data: {}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.isPopupOpened = false;
      });
    }
  }

