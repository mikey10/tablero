import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent} from '@angular/material';
import { ProyectoService } from '../../servicios/proyecto.service';
import { Proyecto } from 'src/app/metodos/proyecto';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AreaService } from '../../servicios/area.service';
import { Area } from 'src/app/metodos/area';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, flatMap } from 'rxjs/operators';
@Component({
  selector: 'app-valtaproyecto',
  templateUrl: './valtaproyecto.component.html',
  styleUrls: ['./valtaproyecto.component.css']
})
export class ValtaproyectoComponent implements OnInit {
  proyecto: Proyecto = new Proyecto();
  area: Area = new Area();
  autocompleteControl = new FormControl();
  areaFiltrados: Observable<Area[]>;
  startDate = new Date(2020, 0, 1);
  vlarea: any;
  vlnuevoItem: any;
  filterValue: any;
  // tslint:disable-next-line:variable-name
  // tslint:disable-next-line:max-line-length
  constructor(private vlServicioProyecto: ProyectoService, private vlAreaArreglo: AreaService, private dialogRef: MatDialogRef<ValtaproyectoComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private vlRoute: Router) { }

  onNoClick(): void {
    this.dialogRef.close();
   }

  ngOnInit() {
    this.areaFiltrados = this.autocompleteControl.valueChanges
      .pipe(
       map(value => typeof value === 'string' ? value : value.fc_descripcion),
        /*startWith(''),*/
        flatMap(value => value ? this._filter(value) : []));
  }

  private _filter(value: string): Observable<Area[]> {
     this.filterValue = value.toLowerCase();

     return this.vlAreaArreglo.getAreaBuscar(this.filterValue);
  }
  mostrarNombre(area?: Area): string | undefined {
     return area ? area.fc_descripcion : undefined;
  }

  SeleccionarArea(event: MatAutocompleteSelectedEvent): void {
     this.vlarea = event.option.value as Area;
     console.log('datos area', this.vlarea);
     this.vlnuevoItem = new Area();
     this.vlnuevoItem.fi_id_area = this.vlarea.fi_id_area;
     /*this.proyecto.area.push(this.vlnuevoItem);*/
     this.autocompleteControl.setValue('');
     event.option.focus();
     event.option.deselect();
  }
   crearProyecto(): void {
    this.proyecto.fi_area = this.vlnuevoItem.fi_id_area;
    console.log('id area se agrega al campo de fi_area del objeto proyecto', this.vlnuevoItem.fi_id_area);
    this.vlServicioProyecto.crearProyecto(this.proyecto).subscribe(
      ((proyecto: {}) => {
        this.vlRoute.navigate(['/proyecto']);
        Swal.fire({
          title: 'Alta',
          text: `El Proyecto ${this.proyecto.fc_nombre}, se registro en el sistema`,
          type: 'success'
        });
        console.log(this.proyecto);
      })
      );
    this.dialogRef.close();
    }
  }
