import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValtaproyectoComponent } from './valtaproyecto.component';

describe('ValtaproyectoComponent', () => {
  let component: ValtaproyectoComponent;
  let fixture: ComponentFixture<ValtaproyectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValtaproyectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValtaproyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
