import { Component, OnInit, Inject, Input } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Area } from '../metodos/area';
import { AreaService } from '../servicios/area.service';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {
@Input() vlArea: Area = new Area();
startDate = new Date(2020, 0, 1);
  // tslint:disable-next-line:max-line-length
  constructor( private vlservice: AreaService, private vlRoute: Router, public dialogRef: MatDialogRef<AreaComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
  onNoClick(): void {
    this.dialogRef.close();
   }

  ngOnInit() {
  }
  createArea(): void {
     this.vlservice.createArea(this.vlArea).subscribe(
       (area: {}) => {
         this.vlRoute.navigate(['/proyecto']);
         console.log(this.vlArea);
         Swal.fire({
          title: 'Alta',
          text: `El area ${this.vlArea.fc_descripcion}, se registro en el sistema`,
          type: 'success'
        });
      });
     this.dialogRef.close();
    }
 }
