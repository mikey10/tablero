import { Area } from './area';
export class Proyecto {
     area: Area [] = [];
     // tslint:disable-next-line:variable-name
     fi_id_proyecto: number;
    // tslint:disable-next-line:variable-name
    fi_id_subproyecto: number;
    // tslint:disable-next-line:variable-name
    fc_nombre: string;
    // tslint:disable-next-line:variable-name
    fi_area: number;
    // tslint:disable-next-line:variable-name
    fi_id_cto_costo: number;
    // tslint:disable-next-line:variable-name
    fi_id_requerimiento: number;
    // tslint:disable-next-line:variable-name
    fd_fecha_alta: string;
    // tslint:disable-next-line:variable-name
    fc_usuario_alta: string;
    fcusuariomodifica: string;
}
