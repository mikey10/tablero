export class Tablero {
    fcdescripcion: string;
    fiproyeto: number;
    fdfechainicio: string;
    fdfechafin: string;
    fitarea: number;
    fistatustarea: number;
    fistatus: number;
    fcsuario: string;
    fcusuariomodifica: string;
}
