import { Proyecto } from './proyecto';

export class Area {
    proyecto: Proyecto [] = [];
      // tslint:disable-next-line:variable-name
    fi_id_area: number;
    // tslint:disable-next-line:variable-name
    fc_descripcion: string;
    // tslint:disable-next-line:variable-name
    fc_usuario_alta: string;
    // tslint:disable-next-line:variable-name
    fd_fecha_alta: string;
}
