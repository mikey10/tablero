import { Proyecto } from './../metodos/proyecto';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import Swal from 'sweetalert2';
import { URL_BACKEND } from '../configuracion/endpoint';

@Injectable({
  providedIn: 'root'
})
export class ProyectoService {
  private urlEndPoint = URL_BACKEND + '/proyectos';
  /*definir las cabeceras */
private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  constructor( private vlcteHttpCliente: HttpClient, private vlRouter: Router) { }

  getProyecto(): Observable<any> {
     return this.vlcteHttpCliente.get(`${this.urlEndPoint}`).pipe(
       map((rs) => rs as Proyecto[]));
  }
  crearProyecto(proyecto: Proyecto) {
    return this.vlcteHttpCliente.post<Proyecto>(this.urlEndPoint, proyecto, {headers: this.httpHeaders});
  }
}
