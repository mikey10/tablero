import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import { URL_BACKEND } from '../configuracion/endpoint';
import { Area } from '../metodos/area';

@Injectable({
  providedIn: 'root'
})
export class AreaService {
  private urlEndPoint = URL_BACKEND + '/areas';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  constructor(private vlHttpClient: HttpClient, private vlRouter: Router) { }

  getArea(): Observable<any> {
    return this.vlHttpClient.get(`${this.urlEndPoint}`).pipe(
      map((rs) => rs as Area[]));
  }
  /*Se obtiene area como arreglo*/
  getAreaBuscar(vlbuscar: string): Observable<Area[]> {
    return this.vlHttpClient.get<Area[]>(`${this.urlEndPoint}/filtrar-area/${vlbuscar}`);
  }
  /*Alta area*/
  createArea(area: Area): Observable<Area> {
    return this.vlHttpClient.post<Area>(this.urlEndPoint, area, {headers: this.httpHeaders});
  }
}
