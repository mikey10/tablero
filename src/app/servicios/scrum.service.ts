import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import Swal from 'sweetalert2';
import { URL_BACKEND } from '../configuracion/endpoint';
import { Scrum } from '../metodos/scrum';
@Injectable({
  providedIn: 'root'
})
export class ScrumService {
  private urlEndPoint = URL_BACKEND;
  /*definir las cabeceras */
private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  constructor( private vlHttpClient: HttpClient, vlRouter: Router) { }

  getScrum(): Observable<any> {
    return this.vlHttpClient.get(`${this.urlEndPoint}`).pipe(
      map((rs) => rs as Scrum[]));
  }
}
