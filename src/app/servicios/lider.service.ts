import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import Swal from 'sweetalert2';
import { URL_BACKEND } from '../configuracion/endpoint';
import { Lider } from '../metodos/lider';

@Injectable({
  providedIn: 'root'
})
export class LiderService {
  private urlEndPoint = URL_BACKEND + '/gerentes';
/*definir las cabeceras */
private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  constructor(private vlcteHttpClient: HttpClient, private vlRouter: Router) { }

  getLider(): Observable<any> {
     return this.vlcteHttpClient.get(`${this.urlEndPoint}`).pipe(
       map((rs) => rs as Lider[])
     );
  }
  /*servicio para registro de configuracion lider*/
  createLider(lider: Lider): Observable<Lider> {
    return this.vlcteHttpClient.post<Lider>(this.urlEndPoint, lider, {headers: this.httpHeaders});
  }
}
