import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/*Rutas*/
import { AppRoutingModule } from './app-routing.module';

/*Modulos*/
import { AppComponent } from './app.component';
/* Servicios*/
import {HttpClientModule} from '@angular/common/http';

/*Agregar los imports de material*/
import { MatAutocompleteModule , MatPaginatorModule, MatSortModule, MatTableModule , MatInputModule  } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import * as Material from '@angular/material';

/*Agregar clase de formulario*/
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProyectoComponent } from './vproyecto/proyecto/proyecto.component';
import { MosaicoComponent } from './vprincipal/mosaico/mosaico.component';
import { CatalogosComponent } from './vcatalogos/catalogos/catalogos.component';
import { LiderComponent } from './vlider/lider.component';
import { ValtaproyectoComponent } from './vproyecto/alta/valtaproyecto.component';
import { LiderService } from './servicios/lider.service';
import { AreaComponent } from './varea/area.component';
import { TareasComponent } from './vtareas/tareas.component';




@NgModule({
  declarations: [
    AppComponent,
    ProyectoComponent,
    MosaicoComponent,
    CatalogosComponent,
    LiderComponent,
    ValtaproyectoComponent,
    AreaComponent,
    TareasComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatDialogModule,
    MatFormFieldModule,
    MatCardModule,
    Material.MatToolbarModule,
    Material.MatGridListModule,
    Material.MatFormFieldModule,
    Material.MatInputModule,
    Material.MatRadioModule,
    Material.MatSelectModule,
    Material.MatCheckboxModule,
    Material.MatDatepickerModule,
    Material.MatNativeDateModule,
    Material.MatButtonModule,
    Material.MatSnackBarModule,
    Material.MatTableModule,
    Material.MatIconModule,
    Material.MatPaginatorModule,
    Material.MatSortModule,
    Material.MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule
  ],
  exports: [
    Material.MatToolbarModule,
    Material.MatGridListModule,
    Material.MatFormFieldModule,
    Material.MatInputModule,
    Material.MatRadioModule,
    Material.MatSelectModule,
    Material.MatCheckboxModule,
    Material.MatDatepickerModule,
    Material.MatNativeDateModule,
    Material.MatButtonModule,
    Material.MatSnackBarModule,
    Material.MatTableModule,
    Material.MatIconModule,
    Material.MatPaginatorModule,
    Material.MatSortModule,
    Material.MatDialogModule,

  ],
  providers: [LiderService],
  entryComponents: [LiderComponent, AreaComponent, ValtaproyectoComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
