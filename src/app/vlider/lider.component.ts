import { Component, OnInit, Inject, Input } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { LiderService } from '../servicios/lider.service';
import { Lider } from '../metodos/lider';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-lider',
  templateUrl: './lider.component.html',
  styleUrls: ['./lider.component.css']
})
export class LiderComponent implements OnInit {
  @Input() vllider: Lider = new Lider();
  public contactForm: FormGroup;
  startDate = new Date(2020, 0, 1);
  // tslint:disable-next-line:max-line-length
  constructor(public vlservice: LiderService, private vlRoute: Router, public dialogRef: MatDialogRef<LiderComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
   }

  ngOnInit() {
  }

  onSubmit() {
      this.dialogRef.close();
   }

   createLider(): void {
      this.vlservice.createLider(this.vllider)
      .subscribe((lider: {}) => {
        this.vlRoute.navigate(['/proyecto']);
        console.log(this.vllider);
        Swal.fire({
          title: 'Alta',
          text: `El Lider ${this.vllider.fc_nombre}, se registro en el sistema`,
          type: 'success'
        });
        console.log(this.vllider);
      });
    }
   }
