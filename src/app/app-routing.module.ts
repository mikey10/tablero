import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { ProyectoComponent } from './vproyecto/proyecto/proyecto.component';
import { MosaicoComponent } from './vprincipal/mosaico/mosaico.component';
import { CatalogosComponent } from './vcatalogos/catalogos/catalogos.component';
import { TareasComponent } from './vtareas/tareas.component';


const routes: Routes = [
  {path: '', redirectTo: '/mosaico', pathMatch: 'full'},
  {path: 'mosaico', component: MosaicoComponent},
  {path: 'proyecto', component: ProyectoComponent},
  {path: 'catalogos', component: CatalogosComponent},
  {path: 'tareas', component: TareasComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

